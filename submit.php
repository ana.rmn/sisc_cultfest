<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$email = "";
$facultate = "";
$birth = "";
$activitate = "";
$error = 0;
$error_text = "";
$i = 0;
$con="";


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}


if(isset($_POST['email'])){
	$email = $_POST['email'];
}


if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['activitate'])){
	$activitate = $_POST['activitate'];
}


if(empty($firstname) || empty($lastname) || empty($email) || empty($birth)  || empty($facultate)){
	$error = 1;
	$error_text = "One or more fields are empty!"; }



if(strlen($firstname) < 3 || strlen($lastname) < 3) {
	$error = 1;
	$error_text = "First or Last name is shorter than expected!"; }



if(strlen($facultate) < 3) {
	$error = 1;
	$error_text = "College name is shorter than expected!"; }


if(strlen($facultate) > 30) {
	$error = 1;
	$error_text = "College name is longer than expected!"; }

	if(strlen($firstname) > 20 ){ 
	$error = 1;
	$error_text = "Firstname is longer than expected!"; }

	if(strlen($lastname) > 20 ) {
		$error = 1;
	$error_text = "Lastname is longer than expected!";}

for ($i=0; $i<strlen($facultate); $i++)
if($facultate[$i]>='0' && $facultate[$i]<='9')
	{$error = 1;
	$error_text = "College name is not valid"; }


for ($i=0; $i<strlen($firstname); $i++)
if($firstname[$i]>='0' && $firstname[$i]<='9')
	{$error = 1;
$error_text = "Firstname is not valid";}


for ($i=0; $i<strlen($lastname); $i++)
if($lastname[$i]>='0' && $lastname[$i]<='9')
	{$error = 1;
$error_text = "Lastname is not valid";}


if(!(filter_var($email, FILTER_VALIDATE_EMAIL)) )
     { $error = 1;
	$error_text = "Email is not valid"; }

while ($i<strlen($email) && $email[$i]!='@')
	{if (!($email[$i]>='a' && $email[$i]<='z' || $email[$i]>='0' && $email[$i]<='9' || $email[$i]=='.' || $email[$i]=='-' || $email[$i]=='_' )) 
  { $error = 1;
	$error_text = "Email is not valid"; }
$i++;}

$age = date_diff(date_create($birth), date_create('now'))->y;
if ($age < 18 ) { $error = 1;
	$error_text = "Too young";} 
if ($age >100 ) { $error = 1;
	$error_text = "Too old";} 

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);
    
} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    

        echo $response;
    return;

}

if($error == 0)
	{ 


$stmt2 = $con -> prepare("INSERT INTO formular(firstname,lastname,email,birth,activitate,facultate) VALUES(:firstname,:lastname,:email,:birth,:activitate, :facultate)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':activitate',$activitate);
$stmt2 -> bindParam(':facultate',$facultate);

    if(!$stmt2->execute()) {
    	$errors['connection'] = "Database Error";
                            }


    else {
      echo "Succes";
          }
                 }

else { 
	echo $error_text;
	return;
      }

 ?>